package graph.hackerland;

import java.io.File;
import java.util.*;

/**
 * This class solves the challenge on https://www.hackerrank.com/challenges/torque-and-development/problem
 * called roads and libraries. On the challenge you need to change scanner to read from System.in.
 * It uses an adjacency list to store the graph and finds a the smallest number of spanning trees
 * to compute the cost of building libraries and roads. If the cost of building a library is less than
 * the cost of building a road, we return the number of cities * cost per library.
 */
public class Hackerland {
    private static List<Set<Integer>> graph;
    private static int numCities, numRoads, librariesToFix, roadsToFix;
    private static long costPerLibrary, costPerRoad;
    private static boolean[] visited;

    public static void main(String[] args) {
        try{
            Scanner in = new Scanner(System.in);
            int q = in.nextInt();
            for(int i = 0; i < q; i++){
                numCities = in.nextInt();
                numRoads = in.nextInt();
                costPerLibrary = in.nextLong();
                costPerRoad = in.nextLong();
                initializeGraph(in);
                long cost = getCost();
                System.out.println(cost);
            }
            in.close();
        }catch(Exception e){

        }
    }

    private static long getCost(){
        if(costPerLibrary < costPerRoad) return costPerLibrary * numCities;
        visited = new boolean[numCities];
        librariesToFix = 0;
        roadsToFix = 0;
        for(int city = 0; city < numCities; city++){
            if(!visited[city]) visit(city);
        }

        return librariesToFix* costPerLibrary + roadsToFix* costPerRoad;
    }

    private static void visit(int city){
        visited[city] = true;
        visitAllNodes(city);
        librariesToFix++;
    }

    private static void visitAllNodes(int city) {
        for(Integer c : graph.get(city)){
            if(!visited[c]){
                visited[c] = true;
                visitAllNodes(c);
                roadsToFix++;
            }
        }
    }

    private static void initializeGraph(Scanner in){
        graph = new ArrayList<>();
        for(int i = 0; i < numCities; i++){
            graph.add(new HashSet<>());
        }
        for(int i = 0; i < numRoads; i++){
            int city_1 = in.nextInt()-1;
            int city_2 = in.nextInt()-1;
            graph.get(city_1).add(city_2);
            graph.get(city_2).add(city_1);
        }
    }
}
