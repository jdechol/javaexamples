package graph.shortestPath.dijkstra;

import java.io.File;
import java.util.Scanner;

public class Graph {
    Scanner scanner;
    int N, M, Q, S;
    int[][] G;

    public Graph(){
        String path = "./resources/shortestpath1.in";
        File file = new File(path);
        try{
        scanner = new Scanner(file);
        readInput();
        scanner.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void readInput() {
        while(setDataLine()){
            generateGraph();
            Dijkstra dijkstra = new Dijkstra(G, S, N);
            getDistances(dijkstra);
            System.out.println();
        }
    }

    private void getDistances(Dijkstra dijkstra) {
        for(int i = 0; i < Q; i++){
            int v = scanner.nextInt();
            int distance = dijkstra.getDistanceFromStart(v);
            if(distance > -1) System.out.println(distance);
            else System.out.println("Impossible");
        }
    }

    private void generateGraph() {
        G = new int[N][N];
        for(int i = 0; i < N; i++){
            for(int j = 0; j < N; j++){
                G[i][j] = -1;
            }
        }
        getEdges();

    }

    private void getEdges() {
        for(int i = 0; i < M; i++){
            int u = scanner.nextInt();
            int v = scanner.nextInt();
            int w = scanner.nextInt();
            G[u][v] = w;
        }
    }

    private boolean setDataLine() {
        N = scanner.nextInt();
        M = scanner.nextInt();
        Q = scanner.nextInt();
        S = scanner.nextInt();
        return N != 0 || M != 0 || Q != 0 || S != 0;
    }
}
