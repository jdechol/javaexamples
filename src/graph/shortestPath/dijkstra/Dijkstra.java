package graph.shortestPath.dijkstra;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class Dijkstra {
    int[][] graph;
    int size, start;
    int[] prev, dist;
    PriorityQueue<Node> queue;
    Map<Integer, Node> lookupTable;

    public Dijkstra(int[][] graph, int start, int size){
        this.lookupTable = new HashMap<>();
        this.graph = graph;
        this.start = start;
        this.size = size;
        initializeArrays();
        generateShortestPath();
    }

    private void initializeArrays() {
        prev = new int[size];
        dist = new int[size];
        for(int i = 0; i < size; i++){
            prev[i] = -1;
            dist[i] = Integer.MAX_VALUE;
        }
        prev[start] = start;
        dist[start] = 0;
    }

    private void generateShortestPath() {
        queue = new PriorityQueue<>(size, new Node());
        addNode(start, 0);

        while(!queue.isEmpty()){
            int u = queue.remove().node;
            findEdgesToUpdate(u);
        }
    }

    private void addNode(int v, int weight){
        Node node = new Node(v, weight);
        lookupTable.put(v, node);
        queue.add(node);
    }

    private void findEdgesToUpdate(int u) {
        for(int i = 0; i < size; i++){
            int v = i;
            if(isEdge(u,v)){
               updateEdge(u,v);
            }
        }
    }
    private boolean isEdge(int u, int v){
        return graph[u][v] > -1;
    }

    private void updateEdge(int u, int v){
        int weight = graph[u][v];
        if(dist[v] > dist[u] + weight){
            dist[v] = dist[u] + weight;
            prev[v] = u;

            addNode(v, dist[v]);
        }
    }

    public int getDistanceFromStart(int v){
        if(prev[v] > -1) return dist[v];
        return -1;
    }

    private class Node implements Comparator<Node>{
       private int node;
       private int weight;

       public Node(int node, int weight){
           this.node = node;
           this.weight = weight;
       }

       public Node(){

       }

       @Override
       public int compare(Node node1, Node node2) {
           if(node1.weight < node2.weight) return -1;
           if(node1.weight > node2.weight) return 1;
           return 0;
       }
   }
}
