package heap;

public class MaxHeap {
    int[] heap;
    int length = 0;

    public MaxHeap(int[] nums){
        heap = new int[nums.length];
        createHeap(nums);
    }

    private void createHeap(int[] nums){
        for(int i = 0; i < nums.length; i++){
            add(nums[i]);
        }
    }

    public void add(int x){
        doubleSizeIfNeeded();
        heap[length++] = x;
        bubbleUp();
    }

    public int pop(){
        swap(0, length-1);
        int pop = heap[--length];
        bubbleDown();
        if(length-1 < heap.length / 2) condenceHeap();
        return pop;
    }

    private void condenceHeap() {
        int[] nHeap = new int[length];
        for(int i = 0; i < length; i++){
            nHeap[i] = heap[i];
        }
        heap = nHeap;
    }

    private void bubbleDown() {
        int index = 0;
        while(hasRightChild(index) && smallerThanChildren(index)){
            int left = getLeftChild(index);
            int right = getRightChild(index);
            if(heap[left] >= heap[right]){
                swap(index, left);
                index = left;
            }
            else{
                swap(index, right);
                index = right;
            }
        }
        if(hasLeftChild(index)){
            int left = getLeftChild(index);
            if(heap[left] > heap[index]) swap(index, left);
        }
    }

    private boolean smallerThanChildren(int index) {
        int left = getLeftChild(index);
        int right = getRightChild(index);
        return heap[index] < heap[left] || heap[index] < heap[right];
    }

    private int getLeftChild(int N) {
        return 2*N + 1;
    }

    private int getRightChild(int N){
        return 2*N + 2;
    }

    private boolean hasLeftChild(int N) {
        return 2*N + 1 < length;
    }

    private boolean hasRightChild(int N) {
        return 2*N + 2 < length;
    }

    private void bubbleUp() {
        int position = length-1;
        int parentPosition = getParent(position);
        while(isLargerThanParent(position, parentPosition)){
            swap(position, parentPosition);
            position = parentPosition;
            parentPosition = getParent(position);
        }
    }

    private boolean isLargerThanParent(int position, int parentPosition){
        return position > 0 && heap[position] > heap[parentPosition];
    }

    private void swap(int a, int b) {
        int temp = heap[a];
        heap[a] = heap[b];
        heap[b] = temp;
    }

    private int getParent(int position) {
        return (position-1)/2;
    }

    private void doubleSizeIfNeeded(){
        if(length == heap.length){
            int[] nHeap = new int[length * 2];
            for(int i = 0; i < heap.length; i++){
                nHeap[i] = heap[i];
            }
            heap = nHeap;
        }
    }
}
