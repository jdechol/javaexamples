package sorting;

import java.util.Arrays;

public class Init {
    public static void main(String[] args){
        int[] numbers = {5,5,1,10,20,2,25,18,-3};
        Sort sort = new MergeSort();
        numbers = sort.sort(numbers);
        System.out.println(Arrays.toString(numbers));
    }

}
