package sorting;

public class QuickSort implements Sort{

    @Override
    public int[] sort(int[] arr) {
        int[] A = arr.clone();
        quickSort(A, 0, A.length-1);
        return A;
    }

    private void quickSort(int[] A, int lo, int hi) {
        if(lo < hi){
            int index = partition(A, lo, hi);
            quickSort(A, lo, index-1);
            quickSort(A, index+1, hi);
        }
    }

    private int partition(int[] A, int lo, int hi) {
        int index = lo;
        swap(A, index, hi);
        for(int i = lo; i < hi; i++) {
            if (A[i] <= A[hi]) swap(A, index++, i);
        }
        swap(A, index, hi);
        return index;
    }

    private void swap(int[] A, int left, int right) {
        int temp = A[left];
        A[left] = A[right];
        A[right] = temp;
    }
}
