package sorting;

public class MergeSort implements Sort{
    int[] A;
    int[] B;

    @Override
    public int[] sort(int[] arr) {
        A = arr.clone();
        B = arr.clone();

        mergeSort(0, A.length - 1);
        return A;
    }

    private void mergeSort(int lo, int hi) {
        if(lo < hi){
            int mid = (hi + lo) / 2;
            mergeSort(lo, mid);
            mergeSort(mid+1, hi);
            merge(lo, mid, hi);
        }
    }

    private void merge(int left, int mid, int right) {
        for(int i = left; i <= right; i++){
            B[i] = A[i];
        }

        int leftPtr = left;
        int rightPtr = mid + 1;
        int index = left;

        while(leftPtr <= mid || rightPtr <= right){
            if(leftPtr > mid){
                A[index] = B[rightPtr++];
            }
            else if (rightPtr > right){
                A[index] = B[leftPtr++];
            }
            else{
                if(B[leftPtr] <= B[rightPtr]) A[index] = B[leftPtr++];
                else A[index] = B[rightPtr++];
            }
            index++;
        }
    }
}
