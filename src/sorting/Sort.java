package sorting;

import java.util.List;

public interface Sort {

    public int[] sort(int[] list);

}
