package tree.trie;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class Node {
    Map<Character, Node>  chars = new HashMap<>();

    public Node addChar(char c){
        if(chars.containsKey(c)) return chars.get(c);
        chars.put(c, new Node());
        return chars.get(c);
    }

    public boolean containsChar(char c){
        return chars.containsKey(c);
    }

    public Node getChild(char c) throws NoSuchElementException{
        if(chars.containsKey(c)) return chars.get(c);

        throw new NoSuchElementException("No such node!");
    }

    public boolean isLeaf(){
        return chars.size() == 0;
    }
}
