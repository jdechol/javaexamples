package tree.trie;

import java.util.Arrays;
import java.util.List;

public class Init {
    public static void main(String[] args){
        List<String> words = Arrays.asList("hello", "goodbye", "hell");
        Trie trie = new Trie(words);
        System.out.println(trie.wordInTree("he"));
    }
}
