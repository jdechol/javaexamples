package tree.trie;

import java.util.List;

public class Trie {
    Node root;
    public Trie(List<String> words){
        root = new Node();
        for(String word : words){
            addWord(word);
        }
    }

    public void addWord(String word) {
        char[] chars = word.toCharArray();
        Node node = root;
        for(char c : chars){
            node = node.addChar(c);
        }
    }

    public boolean wordInTree(String word){
        char[] chars = word.toCharArray();
        Node node = root;
        for(char c : chars){
            if(!node.isLeaf() && node.containsChar(c)) node = node.getChild(c);
            else return false;
        }
        return true;
    }
}
