package tree.binary.balanced;

/**
 * This class is following the same logic as the RedBlackTree that
 * Robert Sedgewick wrote that is published online. I am trying to
 * understand it at this point.
 * @param <Key>
 * @param <Value>
 */
public class RedBlackTree<Key extends Comparable<Key>, Value> {
    private Node root;
    private static final boolean RED = true;
    private static final boolean BLACK = false;

    private class Node{
        Key key;
        Value value;
        boolean color;
        Node left, right;

        public Node(Key key, Value value, boolean color){
            this.key = key;
            this.value = value;
            this.color = color;
        }
    }

    public void put(Key key, Value value) throws IllegalArgumentException{
        if(key == null || value == null) throw new IllegalArgumentException("Invalid arguments cannot be null.");

        root = put(root, key, value);
        root.color = BLACK;
    }

    private Node put(Node r, Key key, Value value){
        if(r == null) return new Node(key, value, RED);

        int compare = key.compareTo(r.key);

        if      (compare <  0)  r.left = put(r.left, key, value);
        else if (compare >  0)  r.right = put(r.right, key, value);
        else                    r.value      = value;

        return balanceTree(r);
    }

    private Node balanceTree(Node r) {
        if (isRed(r.right) && !isRed(r.left))      r = rotateLeft(r);
        if (isRed(r.left)  &&  isRed(r.left.left)) r = rotateRight(r);
        if (isRed(r.left)  &&  isRed(r.right))     flipColors(r);

        return r;
    }

    private void flipColors(Node r) {
        r.color = !r.color;
        r.left.color = !r.left.color;
        r.right.color = !r.right.color;
    }

    private Node rotateRight(Node oldTop) {
        Node top = oldTop.left;
        oldTop.left = top.right;
        top.right = oldTop;
        top.color = BLACK;
        top.right.color = RED;
        return top;
    }

    private Node rotateLeft(Node oldTop) {
        Node top = oldTop.right;
        oldTop.right = top.left;
        top.left = oldTop;
        top.color= BLACK;
        top.left.color = RED;
        return top;
    }

    private boolean isRed(Node node){
        if(node == null) return false;
        return node.color == RED;
    }
}
