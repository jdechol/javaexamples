package hashtable;

public class Init {
    public static void main(String[] args){
        HashTable<String, Integer> table = new HashTable<>();
        table.put("hi", 3);
        table.put("bye", 5);
        table.put("good stuff", 5);
        table.put("good stuff", 6);

        for(String s : table){
            System.out.println(s + " " + table.get(s));
        }

    }
}
