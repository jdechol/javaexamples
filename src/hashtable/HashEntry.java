package hashtable;

public class HashEntry<K, V> {
    private K key;
    private V value;

    public HashEntry(K key, V value){
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    @Override
    public int hashCode(){
        return Math.abs(this.key.hashCode());
    }

    @Override
    public boolean equals(Object o){
        return this.hashCode() == Math.abs(o.hashCode());
    }
}
