package hashtable;

import java.util.Iterator;

public class HashTable <K, V> implements Iterable<K>{
    private HashEntry[] hashEntries;
    private int size;
    private int numEntries = 0;

    public HashTable(){
        this.size = 2;
        this.hashEntries = new HashEntry[size];
    }

    public HashTable(int size){
        this.size = size;
    }

    public void put(K key, V value){
        if(numEntries == size) increaseSize();
        addEntry(key, value);
        numEntries++;
    }

    public V get(K key){
        int index = getIndex(key);
        return (V) this.hashEntries[index].getValue();
    }

    public boolean containsKey(K key){
        int index = getIndex(key);
        if (index == size) return false;
        return true;
    }

    private int getIndex(K key) {
        long hash = getHash(key);
        int index = (int) hash % size;
        while (index < size && !this.hashEntries[index].equals(key)) index++;

        return index;
    }

    private void addEntry(K key, V value) {
        long hash = getHash(key);
        int index =(int) hash % size;
        while(hashEntries[index] != null && !hashEntries[index].equals(key)) index++;
        hashEntries[index] = new HashEntry(key, value);
    }

    private void increaseSize() {
        size *= 2;
        HashEntry[] entries = new HashEntry[size];
        for(HashEntry entry : hashEntries){
            int hash = entry.hashCode();
            int index = hash % size;
            while(entries[index] != null) index++;

            entries[index] = entry;
        }
        hashEntries = entries;
    }

    private long getHash(K key){
        return Math.abs(key.hashCode());
    }

    @Override
    public Iterator<K> iterator() {
        return new Iterator<K>() {
            private int index = 0;

            @Override
            public boolean hasNext() {
                int pos = index;
                while(pos < size && hashEntries[pos] == null) pos++;
                return pos != size;
            }

            @Override
            public K next() {
                while(index < size && hashEntries[index] == null) index++;
                return (K) hashEntries[index++].getKey();
            }

            @Override
            public void remove(){
                throw new UnsupportedOperationException();
            }
        };
    }
}
